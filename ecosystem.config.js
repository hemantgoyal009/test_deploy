module.exports = {
    apps: [
        {
            name: "test_deploy",
            script: "./bin/www",
            env: {
                PORT: 3000,
                NODE_ENV: "production"
            }
        }
    ],
    deploy: {
        production: {
            user: "ubuntu",
            host: "ec2-35-154-114-224.ap-south-1.compute.amazonaws.com",
            key: "~/.ssh/test_deploy.pem",
            ref: "origin/master",
            repo: "git@bitbucket.org:hemantgoyal009/test_deploy.git",
            path: "/home/ubuntu/test_deploy/",
            "post-deploy":
                "npm install && pm2 startOrRestart ecosystem.config.js"
        }
    }
};